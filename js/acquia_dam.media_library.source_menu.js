(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.acquiaDamDisableSourceMenu = {
    attach: function attach(context, settings) {
      function sourceMenuCheck() {
        let source_field = document.getElementsByClassName('js-acquia-dam-source-field')[0];
        if (
          currentSelection.length === settings.media_library.selection_remaining
        ) {
          source_field.classList.add('disable-select');
        } else {
          source_field.classList.remove('disable-select');
        }
      }
      const currentSelection = Drupal.MediaLibrary.currentSelection;
      const media_library_select_checkbox = once(
        'media_library_select_checkbox',
        '.media-library-item__click-to-select-checkbox input',
      );
      for (let i = 0; i < media_library_select_checkbox.length; i++) {
        $(media_library_select_checkbox[i]).on('change', sourceMenuCheck);
      }
    }
  };
  Drupal.behaviors.MediaLibrarySourceTabs = {
    attach: function attach(context) {
      const source_field = once(
        'js-acquia-dam-source-field',
        '.js-acquia-dam-source-field',
        context,
      ).shift();
      if (source_field) {
        source_field.addEventListener('change', function (e) {
          // Prevent the user to re-trigger the ajax while on progress.
          e.currentTarget.classList.add('disable-select')
          const ajaxObject = Drupal.ajax({
            wrapper: 'media-library-wrapper',
            url: drupalSettings.media_library[e.target.value],
            dialogType: 'ajax',
            progress: {
              type: 'throbber',
            },
          });
          ajaxObject.execute();
        });
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
