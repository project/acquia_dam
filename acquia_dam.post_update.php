<?php

/**
 * @file
 * Post update functions for Acquia Dam.
 */

declare(strict_types=1);

use Drupal\acquia_dam\Entity\MediaSourceField;
use Drupal\acquia_dam\Plugin\Field\FieldType\AssetItem;
use Drupal\Core\Database\Database;

/**
 * Populate the external_id field.
 */
function acquia_dam_post_update_existing_media_external_id_field(&$sandbox) {
  // Populate existing media.
  $media_ids = \Drupal::entityTypeManager()->getStorage('media')
    ->getQuery()
    ->accessCheck(TRUE)
    ->condition(MediaSourceField::SOURCE_FIELD_NAME, NULL, 'IS NOT NULL')
    ->execute();

  if (empty($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['media_ids'] = array_values($media_ids);
    $sandbox['max'] = count($media_ids);
  }

  $start = $sandbox['progress'];
  $end = min($sandbox['max'], $start + 10);

  for ($i = $start; $i < $end; $i++) {
    $media_entity_id = $sandbox['media_ids'][$i];

    /** @var \Drupal\media\MediaInterface $media_entity */
    $media_entity = \Drupal::entityTypeManager()->getStorage('media')
      ->load($media_entity_id);

    $source_field = $media_entity->get(MediaSourceField::SOURCE_FIELD_NAME);
    if ($source_field->isEmpty()) {
      continue;
    }

    $source_field_item = $source_field->first();
    assert($source_field_item instanceof AssetItem);
    $source = $media_entity->getSource();

    $source_data = $source->getSourceFieldValue($media_entity);
    if (isset($source_data['external_id']) && $source_data['external_id']) {
      continue;
    }

    $external_id = $source->getMetadata($media_entity, "external_id");
    $media_entity->get(MediaSourceField::SOURCE_FIELD_NAME)->external_id = $external_id;
    $media_entity->save();
  }

  if ($sandbox['max'] > 0 && $end < $sandbox['max']) {
    $sandbox['progress'] = $end;
    $sandbox['#finished'] = ($end - 1) / $sandbox['max'];
  }
  else {
    $sandbox['#finished'] = 1;
  }
}

/**
 * New service definition crop_new_asset_version_subscriber.
 */
function acquia_dam_post_update_add_crop_new_asset_version_subscriber(&$sandbox) {
  // Empty post_update hook to rebuild service container.
}

/**
 * Implements hook_post_update_NAME().
 */
function acquia_dam_post_update_link_tracking_primary_key(&$sandbox) {
  $schema = Database::getConnection()->schema();
  if (!$schema->indexExists('acquia_dam_integration_link_tracking', 'PRIMARY')) {
    if ($schema->indexExists('acquia_dam_integration_link_tracking', 'integration_link_id')) {
      $schema->dropIndex('acquia_dam_integration_link_tracking', 'integration_link_id');
    }
    $schema->addPrimaryKey('acquia_dam_integration_link_tracking', ['integration_link_id']);
  }
}

/**
 * Implements hook_post_update_NAME().
 */
function acquia_dam_post_update_link_tracking_change_primary_key(&$sandbox) {
  $schema = Database::getConnection()->schema();
  if ($schema->indexExists('acquia_dam_integration_link_tracking', 'PRIMARY')) {
    $schema->dropPrimaryKey('acquia_dam_integration_link_tracking');
    $schema->addPrimaryKey('acquia_dam_integration_link_tracking', ['integration_link_id']);
  }
}

/**
 * Force a cache refresh because new services were added.
 */
function acquia_dam_post_update_refresh_container(&$sandbox) {
  // Empty post-update hook.
}
