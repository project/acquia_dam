<?php

namespace Drupal\acquia_dam\Form;

use Drupal\acquia_dam\Exception\DamClientException;
use Drupal\acquia_dam\Exception\DamConnectException;
use Drupal\acquia_dam\Exception\DamServerException;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Acquia DAM module configuration form.
 */
class AcquiaDamMetadataConfigurationForm extends ConfigFormBase {

  /**
   * DAM client factory.
   *
   * @var \Drupal\acquia_dam\Client\AcquiaDamClientFactory
   */
  protected $clientFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->clientFactory = $container->get('acquia_dam.client.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'acquia_dam_metadata_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'acquia_dam.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('acquia_dam.settings');
    $form['configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Acquia DAM Metadata details'),
    ];
    $display_key_options = [];
    try {
      /** @var array<string, mixed> $meta_data_list */
      $meta_data_list = $this->clientFactory->getSiteClient()->getDisplayKeys('all');
      foreach ($meta_data_list['fields'] as $metadata) {
        $display_key_options[$metadata['display_key']] = $metadata['display_name'];
      }
    }
    catch (DamClientException | DamServerException | DamConnectException $e) {
      $this->messenger()->addError($e->getMessage());
      return $form;
    }
    $default_value = array_keys($config->get('allowed_metadata') ?: []);

    if (!empty($display_key_options)) {
      $form['configuration']['metadata'] = [
        '#type' => 'checkboxes',
        '#description' => $this->t('Select which all metadata should be mapped in Acquia DAM media assets.'),
        '#description_display' => 'before',
        '#title' => $this->t('Allowed metadata'),
        '#options' => $display_key_options,
        '#default_value' => $default_value,
        '#size' => 6,
        '#multiple' => TRUE,
      ];
      $form['configuration']['actions']['#type'] = 'actions';
      $form['configuration']['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save metadata configuration'),
        '#button_type' => 'primary',
      ];

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Save the allowed metadata values.
    $checkboxes = $form_state->getValue('metadata') ?? [];
    $selected_values = [];
    $options = $form['configuration']['metadata']['#options'];
    // Storing with labels inorder to build the source mapping correctly.
    foreach (array_keys(array_filter($checkboxes)) as $selections) {
      $selected_values[$selections] = $options[$selections];
    }
    $this->config('acquia_dam.settings')
      ->set('allowed_metadata', array_filter($selected_values))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
