<?php

namespace Drupal\acquia_dam\Controller;

use Drupal\acquia_dam\AssetVersionResolver;
use Drupal\acquia_dam\Client\AcquiaDamClientFactory;
use Drupal\acquia_dam\Entity\MediaSourceField;
use Drupal\acquia_dam\Plugin\Field\FieldType\AssetItem;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\media\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for check updates custom operation.
 */
class VersionCheckController extends ControllerBase {

  /**
   * DAM client instance.
   *
   * @var \Drupal\acquia_dam\Client\AcquiaDamClient
   */
  protected $damClient;

  /**
   * DAM asset update queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $assetUpdateQueue;

  /**
   * DAM asset version resolver.
   *
   * @var \Drupal\acquia_dam\AssetVersionResolver
   */
  protected $assetVersionResolver;

  /**
   * PreviewEntity constructor.
   *
   * @param \Drupal\acquia_dam\Client\AcquiaDamClientFactory $clientFactory
   *   Client factory.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   * @param \Drupal\acquia_dam\AssetVersionResolver $assetVersionResolver
   *   DAM asset version resolver.
   */
  public function __construct(AcquiaDamClientFactory $clientFactory, QueueFactory $queueFactory, AssetVersionResolver $assetVersionResolver) {
    $this->damClient = $clientFactory->getUserClient();
    $this->assetUpdateQueue = $queueFactory->get('acquia_dam_asset_update');
    $this->assetVersionResolver = $assetVersionResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('acquia_dam.client.factory'),
      $container->get('queue'),
      $container->get('acquia_dam.asset_version_resolver'),
    );
  }

  /**
   * Checks for available updates.
   *
   * If there is update available, then it creates a queue item.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media instance to check.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response.
   *
   * @throws \Drupal\acquia_dam\Exception\DamServerException
   */
  public function checkVersionUpdate(MediaInterface $media): RedirectResponse {
    $media_id = $media->id();
    $label = $media->label();
    $source_field_item = $media->get(MediaSourceField::SOURCE_FIELD_NAME)->first();
    assert($source_field_item instanceof AssetItem);
    $asset_ids = $source_field_item->getValue();
    $local_version_id = $asset_ids['version_id'];
    $remote_version_id = $this->assetVersionResolver->getFinalizedVersion($asset_ids['asset_id']);

    if ($local_version_id !== $remote_version_id) {
      $this->assetUpdateQueue->createItem([
        'asset_id' => $asset_ids['asset_id'],
        'media_id' => $media_id,
      ]);

      $this->messenger()->addStatus($this->t('The media item %media_label has a different version set as the finalized one for the DAM asset so it will be fetched soon.', [
        '%media_label' => $label,
      ]));

      return $this->redirect('view.dam_content_overview.page_1');
    }

    $this->messenger()->addStatus($this->t('The media item %media_label has the same version set as the finalized one for the DAM asset.', [
      '%media_label' => $label,
    ]));

    return $this->redirect('view.dam_content_overview.page_1');
  }

}
