<?php

namespace Drupal\acquia_dam\Controller;

use Drupal\acquia_dam\AcquiadamAuthService;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller handling the authentication for Acquia DAM.
 */
class AuthenticationController extends ControllerBase {

  /**
   * The request stack factory service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * DAM auth service.
   *
   * @var \Drupal\acquia_dam\AcquiadamAuthService
   */
  protected $authService;

  /**
   * Acquia DAM logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $damLoggerChannel;

  /**
   * AuthenticationPage construct.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\acquia_dam\AcquiadamAuthService $authService
   *   DAM Authentication service.
   */
  public function __construct(RequestStack $request_stack, AcquiadamAuthService $authService) {
    $this->requestStack = $request_stack;
    $this->authService = $authService;
    $this->damLoggerChannel = $this->getLogger('logger.channel.acquia_dam');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('acquia_dam.authentication_service'),
    );
  }

  /**
   * Callback from Acquia DAM to complete user authorization process.
   */
  public function authenticateUser(): RedirectResponse {
    // Get the code returned by the Acquia DAM API endpoint, if available.
    $code = $this->requestStack->getCurrentRequest()->query->get('code');
    $user_id = $this->requestStack->getCurrentRequest()->query->get('uid');

    $error_message = '';

    if (empty($user_id)) {
      $this->damLoggerChannel->error('User authentication request does not contain user id.');
      $this->messenger()->addError($this->t('Your site has not been authenticated with Acquia DAM.'));
      // Redirect to Acquia DAM config.
      return $this->redirect('acquia_dam.config');
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $this
      ->entityTypeManager()
      ->getStorage('user')
      ->load($user_id);

    if (empty($user)) {
      $this->damLoggerChannel->error('Cannot find user with the given user ID.');
      $error_message = $this->t('Your site has not been authenticated with Acquia DAM.');
    }

    if (empty($code)) {
      $this->damLoggerChannel->error('Authentication request does not contain authentication code.');
      $error_message = $this->t('Your site has not been authenticated with Acquia DAM.');
    }

    if (isset($code) && !empty($user)) {
      try {
        $this->authService->authenticateDam($code, (int) $user->id());
      }
      catch (\Exception $exception) {
        $this->damLoggerChannel->error('Error during user authentication: ' . $exception->getMessage());
        $error_message = $exception->getMessage();
      }
    }

    $error_message ?
      $this->messenger()->addError($error_message) :

      $this->messenger()->addStatus($this->t('Hooray! You can start using media from Acquia DAM.'));

    return $this->redirect('entity.user.acquia_dam_auth', ['user' => $user->id()]);
  }

  /**
   * Callback from Acquia DAM to complete site authorization process.
   */
  public function authenticateSite(): RedirectResponse {
    // Get the code returned by the Acquia DAM API endpoint, if available.
    $code = $this->requestStack->getCurrentRequest()->query->get('code');

    $error_message = '';

    if (empty($code)) {
      $this->damLoggerChannel->error('Authenticate request does not contain authentication code.');
      $error_message = $this->t('Your site has not been authenticated with Acquia DAM.');
    }

    if (isset($code)) {
      try {
        $this->authService->authenticateDam($code);
      }
      catch (\Exception $exception) {
        $this->damLoggerChannel->error('Error during site authentication: ' . $exception->getMessage());
        $error_message = $exception->getMessage();
      }
    }

    $error_message ?
      $this->messenger()->addError($error_message) :
      $this->messenger()->addStatus($this->t('The site has been authenticated with Acquia DAM. <a href=":url">Authenticate your user account too</a>', [
        ':url' => Url::fromRoute('entity.user.acquia_dam_auth', ['user' => $this->currentUser()->id()])->toString(),
      ]));

    return $this->redirect('acquia_dam.config');
  }

  /**
   * Deletes authentication info from user data.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect response instance.
   */
  public function logout(): RedirectResponse {
    $user_id = $this->currentUser()->id();
    $acquia_dam_account = $this->authService->getUserData($user_id);
    try {
      $this->authService->cancelUserRegistration($acquia_dam_account['acquia_dam_token'], (int) $user_id);
    }
    catch (\Exception $exception) {
      $this->messenger()->addMessage('Something went wrong during logout process. Please contact the site administrator for more information.');
      $this->damLoggerChannel->error('Error during logout request: ' . $exception->getMessage());
      return $this->redirect('entity.user.acquia_dam_auth', ['user' => $user_id]);
    }

    $this->messenger()->addMessage($this->t('Your account has been disconnected from Acquia DAM.'));

    return $this->redirect('entity.user.acquia_dam_auth', ['user' => $user_id]);
  }

}
