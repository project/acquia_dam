<?php

declare(strict_types=1);

namespace Drupal\acquia_dam\Plugin\media\Source;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Deriver to create asset media source plugins based on supported asset types.
 */
final class AssetDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [
      'pdf' => [
        'id' => 'pdf',
        'label' => $this->t('PDF (DAM)'),
        'default_thumbnail_filename' => 'generic.png',
        'asset_search_key' => 'ft',
        'asset_search_value' => 'pdf',
      ] + $base_plugin_definition,
      'video' => [
        'id' => 'video',
        'label' => $this->t('Video (DAM)'),
        'default_thumbnail_filename' => 'generic.png',
        'asset_search_key' => 'ft',
        'asset_search_value' => 'video',
      ] + $base_plugin_definition,
      'spinset' => [
        'id' => 'spinset',
        'label' => $this->t('Spinset (DAM)'),
        'default_thumbnail_filename' => 'generic.png',
        'asset_search_key' => 'ff',
        'asset_search_value' => 'SpinSet',
      ] + $base_plugin_definition,
      'image' => [
        'id' => 'image',
        'label' => $this->t('Image (DAM)'),
        'default_thumbnail_filename' => 'generic.png',
        'asset_search_key' => 'ft',
        'asset_search_value' => 'image',
      ] + $base_plugin_definition,
      'documents' => [
        'id' => 'documents',
        'label' => $this->t('Documents (DAM)'),
        'default_thumbnail_filename' => 'generic.png',
        'asset_search_key' => 'ft',
        'asset_search_value' => 'office',
      ] + $base_plugin_definition,
    ];
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
